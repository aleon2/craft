<?php

namespace console\controllers;
use yii\console\Controller;

class TestControl extends Controller {
    public function actionIndex() {
        echo 'cron service running';
    }

    public function actionMail($to) {
        echo 'Sending mail to ' . $to;
    }
}